<?php
/*
jika paramater integer >= 85 dan <= 100 maka akan mereturn String “Sangat Baik” 
jika parameter integer >= 70 dan < 85 maka akan mereturn string “Baik” 
jika parameter number >= 60 dan < 70 maka akan mereturn string “Cukup”
selain itu maka akan mereturn string “Kurang”
*/
echo "<h3>Tentukan Nilai</h3>";
function tentukan_nilai($number)
{
    echo $number . " : " ;
    if($number >= 85 && $number <= 100){
        echo "Sangat baik<br>";
    }else if ($number >= 70 && $number < 85){
        echo "Baik<br>";
    }else if($number >= 60 && $number < 70){
        echo "Cukup<br>";
    }else{
        echo "Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>